# flask

## Dependencies

pip3 install Flask
pip3 install Flask-BasicAuth
pip3 install gunicorn

## Run 
gunicorn --certfile=../cert.pem --keyfile=../key.pem -b <YOURIP>:5000 -w 16 'api:app'
