import json
from flask import Flask, jsonify, request
from flask_basicauth import BasicAuth
from monitor import get_values

app = Flask(__name__)
app.config['BASIC_AUTH_USERNAME'] = 'sysadmin'
app.config['BASIC_AUTH_PASSWORD'] = 'password'
app.config['BASIC_AUTH_FORCE'] = True

basic_auth = BasicAuth(app)

@app.route('/test', methods=['GET'])
def test():
    return get_values(), 200

@app.route('/isAlive', methods=['GET'])
def isAlive():
    return 'Im alive', 200

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=5000,ssl_context=("cert.pem","key.pem"), debug=True)
