import usb.core
import usb.util

def get_values():
    dev = usb.core.find(idVendor=0x0001, idProduct=0x0000)
    output=usb.util.get_string(dev,3,1)
    return output[1:-1].split(" ")
